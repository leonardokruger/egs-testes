using System;
					
public class Program
{
	public static void Main()
	{
		var valor = Decimal.Parse(Console.ReadLine());
		var desconto = Desconto(valor);
		Console.WriteLine(String.Format("Valor do Produto = R$ {0}",valor));
		Console.WriteLine(String.Format("Valor do Desconto = R$ {0}", (valor - desconto).ToString("#.##") ));
		Console.WriteLine(String.Format("Valor com Desconto = R$ {0}", desconto ));
		
	}

	public static decimal Desconto(decimal val ){
		val = val * (1m-0.6m);
		return val;
	}
}


